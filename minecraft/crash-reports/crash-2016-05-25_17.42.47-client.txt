---- Minecraft Crash Report ----
// I bet Cylons wouldn't have this problem.

Time: 25/05/16 17:42
Description: Unexpected error

java.lang.NoSuchMethodError: net.minecraft.client.renderer.texture.ITickable.tick()V
	at cofh.tweak.asmhooks.HooksCore.tickTextures(HooksCore.java:675)
	at net.minecraft.client.Minecraft.func_71407_l(Minecraft.java:1609)
	at net.minecraft.client.Minecraft.func_71411_J(Minecraft.java:973)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:898)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:310)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:395)
	at org.multimc.EntryPoint.listen(EntryPoint.java:170)
	at org.multimc.EntryPoint.main(EntryPoint.java:54)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_91, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 538411944 bytes (513 MB) / 1745879040 bytes (1665 MB) up to 3817865216 bytes (3641 MB)
	Mod Pack: Unknown / None
	LiteLoader Mods: 2 loaded mod(s)
          - VoxelMap version 1.6.10
          - Mouse Tweaks version 2.4.4
	LaunchWrapper: 38 active transformer(s)
          - Transformer: cpw.mods.fml.common.asm.transformers.PatchingTransformer
          - Transformer: com.mumfrey.liteloader.transformers.event.EventProxyTransformer
          - Transformer: com.mumfrey.liteloader.launch.LiteLoaderTransformer
          - Transformer: com.mumfrey.liteloader.client.transformers.CrashReportTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.MarkerTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.SideTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.EventSubscriptionTransformer
          - Transformer: net.minecraftforge.classloading.FluidIdTransformer
          - Transformer: appeng.transformer.asm.ASMIntegration
          - Transformer: myrathi.infinibows.asm.BowTransformer
          - Transformer: invtweaks.forge.asm.ContainerTransformer
          - Transformer: logisticspipes.asm.LogisticsClassTransformer
          - Transformer: li.cil.oc.common.asm.ClassTransformer
          - Transformer: openmods.core.OpenModsClassTransformer
          - Transformer: openblocks.asm.OpenBlocksClassTransformer
          - Transformer: me.guichaguri.betterfps.transformers.MathTransformer
          - Transformer: me.guichaguri.betterfps.transformers.EventTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.DeobfuscationTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.AccessTransformer
          - Transformer: net.minecraftforge.transformers.ForgeAccessTransformer
          - Transformer: appeng.transformer.asm.ASMTweaker
          - Transformer: cofh.asm.CoFHAccessTransformer
          - Transformer: invtweaks.forge.asm.ITAccessTransformer
          - Transformer: logisticspipes.asm.LogisticsAccessTransformer
          - Transformer: net.malisis.core.asm.MalisisCoreAccessTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.ModAccessTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.ItemStackTransformer
          - Transformer: cofh.asm.CoFHClassTransformer
          - Transformer: net.malisis.core.util.chunkcollision.ChunkCollisionTransformer
          - Transformer: net.malisis.core.util.chunkblock.ChunkBlockTransformer
          - Transformer: cofh.tweak.asm.CoFHClassTransformer
          - Transformer: fastcraft.J
          - Transformer: cpw.mods.fml.common.asm.transformers.TerminalTransformer
          - Transformer: com.mumfrey.liteloader.client.transformers.LiteLoaderEventInjectionTransformer
          - Transformer: com.mumfrey.liteloader.client.transformers.MinecraftOverlayTransformer
          - Transformer: com.mumfrey.liteloader.common.transformers.LiteLoaderPacketTransformer
          - Transformer: com.thevoxelbox.voxelmap.litemod.VoxelMapTransformer
          - Transformer: cpw.mods.fml.common.asm.transformers.ModAPITransformer
	JVM Flags: 3 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms512m -Xmx4096m
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP v9.05 FML v7.10.99.99 CoFHTweaks v1.1.3B2 Minecraft Forge 10.13.4.1566 92 mods loaded, 92 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	U	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	U	FML{7.10.99.99} [Forge Mod Loader] (forge-1.7.10-10.13.4.1566-1.7.10-universal.jar) 
	U	Forge{10.13.4.1566} [Minecraft Forge] (forge-1.7.10-10.13.4.1566-1.7.10-universal.jar) 
	U	appliedenergistics2-core{rv2-stable-10} [AppliedEnergistics2 Core] (minecraft.jar) 
	U	InfiniBows{1.3.0 build 20} [InfiniBows] (minecraft.jar) 
	U	OpenComputers|Core{1.5.22.46} [OpenComputers (Core)] (minecraft.jar) 
	U	OpenModsCore{0.9.1} [OpenModsCore] (minecraft.jar) 
	U	<CoFH ASM>{000} [CoFH ASM] (minecraft.jar) 
	U	<CoFH Tweak>{1.7.10R1.1.3B2} [CoFH Tweak ASM] (CoFHTweaks-[1.7.10]1.1.3B2-92-dev.jar) 
	U	FastCraft{1.23} [FastCraft] (fastcraft-1.23.jar) 
	U	ArmorStatusHUD{1.28} [ArmorStatusHUD] ([1.7.10]ArmorStatusHUD-client-1.28.jar) 
	U	StartingInventory{1.7.10.r03} [StartingInventory] ([1.7.10]StartingInventory-universal-1.7.10.r03.jar) 
	U	Treecapitator{1.7.10} [Treecapitator] ([1.7.10]Treecapitator-universal-2.0.4.jar) 
	U	appliedenergistics2{rv2-stable-10} [Applied Energistics 2] (appliedenergistics2-rv2-stable-10.jar) 
	U	BetterAchievements{0.1.0} [Better Achievements] (BetterAchievements-1.7.10-0.1.0.jar) 
	U	BiblioCraft{1.11.5} [BiblioCraft] (BiblioCraft[v1.11.5][MC1.7.10].jar) 
	U	BiblioWoodsForestry{1.5} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.5].jar) 
	U	BiblioWoodsHighlands{1.4} [BiblioWoods Highlands Edition] (BiblioWoods[Highlands][v1.4].jar) 
	U	BiblioWoodsNatura{1.5} [BiblioWoods Natura Edition] (BiblioWoods[Natura][v1.5].jar) 
	U	BuildCraft|Builders{7.1.16} [BC Builders] (buildcraft-7.1.16.jar) 
	U	BuildCraft|Transport{7.1.16} [BC Transport] (buildcraft-7.1.16.jar) 
	U	BuildCraft|Energy{7.1.16} [BC Energy] (buildcraft-7.1.16.jar) 
	U	BuildCraft|Silicon{7.1.16} [BC Silicon] (buildcraft-7.1.16.jar) 
	U	BuildCraft|Robotics{7.1.16} [BC Robotics] (buildcraft-7.1.16.jar) 
	U	BuildCraft|Core{7.1.16} [BuildCraft] (buildcraft-7.1.16.jar) 
	U	BuildCraft|Factory{7.1.16} [BC Factory] (buildcraft-7.1.16.jar) 
	U	BuildCraft|Compat{7.0.11} [BuildCraft Compat] (buildcraft-compat-7.0.11.jar) 
	U	CarpentersBlocks{3.3.7} [Carpenter's Blocks] (Carpenter's Blocks v3.3.7 - MC 1.7.10.jar) 
	U	ChickenChunks{1.3.4.19} [ChickenChunks] (ChickenChunks-1.7.10-1.3.4.19-universal.jar) 
	U	chisel{2.9.5.11} [Chisel] (Chisel-2.9.5.11.jar) 
	U	CoFHCore{1.7.10R3.1.2} [CoFH Core] (CoFHCore-[1.7.10]3.1.2-325.jar) 
	U	CustomMainMenu{1.9.2} [Custom Main Menu] (CustomMainMenu-MC1.7.10-1.9.2.jar) 
	U	enderioaddons{0.10.9} [Ender IO Addons] (EnderIOAddons-1.7.10-2.3.0.425_beta-0.10.9.52_beta.jar) 
	U	EnderStorage{1.4.7.37} [EnderStorage] (EnderStorage-1.7.10-1.4.7.37-universal.jar) 
	U	etfuturum{1.5.5} [Et Futurum] (Et Futurum-1.5.5.jar) 
	U	ExtraTiC{1.4.6} [ExtraTiC] (ExtraTiC-1.7.10-1.4.6.jar) 
	U	Forestry{4.2.12.60} [Forestry for Minecraft] (forestry_1.7.10-4.2.12.60.jar) 
	U	FTBU{1.0.18.2} [FTBUtilities] (FTBUtilities-1.7.10-1.0.18.3.jar) 
	U	Highlands{2.2.3} [Highlands] (Highlands-1.7.2-v-2.2.3.jar) 
	U	ImmersiveEngineering{0.7.7} [Immersive Engineering] (ImmersiveEngineering-0.7.7.jar) 
	U	immersiveintegration{0.6.8} [Immersive Integration] (immersiveintegration-0.6.8.jar) 
	U	inventorytweaks{1.59-dev-152-cf6e263} [Inventory Tweaks] (InventoryTweaks-1.59-dev-152.jar) 
	U	IronChest{6.0.62.742} [Iron Chest] (ironchest-1.7.10-6.0.62.742-universal.jar) 
	U	llor{1.0.7-mc1.7.10} [Light Level Overlay Reloaded] (LLOverlayReloaded-1.0.7-mc1.7.10.jar) 
	U	LogisticsPipes{0.9.3.122} [Logistics Pipes] (logisticspipes-0.9.3.122.jar) 
	U	malisiscore{1.7.10-0.14.3} [MalisisCore] (malisiscore-1.7.10-0.14.3.jar) 
	U	malisisdoors{1.7.10-1.13.2} [Malisis' Doors] (malisisdoors-1.7.10-1.13.2.jar) 
	U	Mantle{1.7.10-0.3.2.jenkins191} [Mantle] (Mantle-1.7.10-0.3.2b.jar) 
	U	Mekanism{9.0.1} [Mekanism] (Mekanism-1.7.10-9.0.1.270.jar) 
	U	MekanismGenerators{9.0.1} [MekanismGenerators] (MekanismGenerators-1.7.10-9.0.1.270.jar) 
	U	MekanismTools{9.0.1} [MekanismTools] (MekanismTools-1.7.10-9.0.1.270.jar) 
	U	MineTweaker3{3.0.10} [MineTweaker 3] (MineTweaker3-1.7.10-3.0.10B.jar) 
	U	modtweaker2{0.9.5} [Mod Tweaker 2] (ModTweaker2-0.9.5.jar) 
	U	powersuits{0.11.0.301} [MachineMuse's Modular Powersuits] (ModularPowersuits-0.11.0.301.jar) 
	U	MouseTweaks{2.4.4} [Mouse Tweaks] (MouseTweaks-2.4.4-mc1.7.10.jar) 
	U	Natura{2.2.0} [Natura] (natura-1.7.10-2.2.0.1.jar) 
	U	NetherOres{1.7.10R2.3.1} [Nether Ores] (NetherOres-[1.7.10]2.3.1-22.jar) 
	U	nmsot{1.1.4-mc1.7.10} [No Mob Spawning on Trees] (NoMobSpawningOnTrees-1.1.4-mc1.7.10.jar) 
	U	OpenBlocks{1.5.1} [OpenBlocks] (OpenBlocks-1.7.10-1.5.1.jar) 
	U	OpenComputers{1.5.22.46} [OpenComputers] (OpenComputers-MC1.7.10-1.5.22.46-universal.jar) 
	U	OpenMods{0.9.1} [OpenMods] (OpenModsLib-1.7.10-0.9.1.jar) 
	U	fodc{1.6.1} [Ore Dictionary Converter] (OreDictionaryConverter-1.6.1.jar) 
	U	ProjRed|Core{4.7.0pre12.95} [ProjectRed Core] (ProjectRed-1.7.10-4.7.0pre12.95-Base.jar) 
	U	ProjRed|Compatibility{4.7.0pre12.95} [ProjectRed Compatibility] (ProjectRed-1.7.10-4.7.0pre12.95-Compat.jar) 
	U	ProjRed|Integration{4.7.0pre12.95} [ProjectRed Integration] (ProjectRed-1.7.10-4.7.0pre12.95-Integration (1).jar) 
	U	ProjRed|Transmission{4.7.0pre12.95} [ProjectRed Transmission] (ProjectRed-1.7.10-4.7.0pre12.95-Integration (1).jar) 
	U	ProjRed|Illumination{4.7.0pre12.95} [ProjectRed Illumination] (ProjectRed-1.7.10-4.7.0pre12.95-Lighting.jar) 
	U	Railcraft{9.12.2.0} [Railcraft] (Railcraft_1.7.10-9.12.2.0.jar) 
	U	RedstoneArsenal{1.7.10R1.1.2} [Redstone Arsenal] (RedstoneArsenal-[1.7.10]1.1.2-92.jar) 
	U	simplyjetpacks{1.5.3} [Simply Jetpacks] (SimplyJetpacks-MC1.7.10-1.5.3.jar) 
	U	StevesCarts{2.0.0.b18} [Steve's Carts 2] (StevesCarts2.0.0.b18.jar) 
	U	StorageDrawers{1.7.10-1.9.8} [Storage Drawers] (StorageDrawers-1.7.10-1.9.8.jar) 
	U	StorageDrawersMisc{1.7.10-1.1.2} [Storage Drawers: Misc Pack] (StorageDrawers-Misc-1.7.10-1.1.2.jar) 
	U	StorageDrawersNatura{1.7.10-1.1.1} [Storage Drawers: Natura Pack] (StorageDrawers-Natura-1.7.10-1.1.1.jar) 
	U	TConstruct{1.7.10-1.8.8.build988} [Tinkers' Construct] (TConstruct-1.7.10-1.8.8.jar) 
	U	ThermalDynamics{1.7.10R1.2.0} [Thermal Dynamics] (ThermalDynamics-[1.7.10]1.2.0-171.jar) 
	U	ThermalExpansion{1.7.10R4.1.2} [Thermal Expansion] (ThermalExpansion-[1.7.10]4.1.2-240.jar) 
	U	ThermalFoundation{1.7.10R1.2.4} [Thermal Foundation] (ThermalFoundation-[1.7.10]1.2.4-114.jar) 
	U	recycling{1.1.0.5} [Thermal Recycling] (ThermalRecycling-1.7.10-1.1.0.5.jar) 
	U	TiCTooltips{1.2.5} [TiC Tooltips] (TiCTooltips-mc1.7.10-1.2.5.jar) 
	U	VeinMiner{0.31.6_build.unknown} [Vein Miner] (VeinMiner-1.7.10_0.31.6.unknown.jar) 
	U	VeinMinerModSupport{0.31.6_build.unknown} [Mod Support] (VeinMiner-1.7.10_0.31.6.unknown.jar) 
	U	Waila{1.5.10} [Waila] (Waila-1.5.10_1.7.10.jar) 
	U	WailaHarvestability{1.1.6} [Waila Harvestability] (WailaHarvestability-mc1.7.10-1.1.6.jar) 
	U	wailaplugins{MC1.7.10-0.2.0-23} [WAILA Plugins] (WAILAPlugins-MC1.7.10-0.2.0-23.jar) 
	U	wawla{1.3.1} [What Are We Looking At] (Wawla-1.0.5.120.jar) 
	U	weaponmod{v1.14.3} [Balkon's WeaponMod] (weaponmod-1.14.3.jar) 
	U	bspkrsCore{6.16} [bspkrsCore] ([1.7.10]bspkrsCore-universal-6.16.jar) 
	U	ForgeMicroblock{1.2.0.345} [Forge Microblocks] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	U	ForgeMultipart{1.2.0.345} [Forge Multipart] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	U	McMultipart{1.2.0.345} [Minecraft Multipart Plugin] (ForgeMultipart-1.7.10-1.2.0.345-universal.jar) 
	U	MrTJPCoreMod{1.1.0.33} [MrTJPCore] (MrTJPCore-1.7.10-1.1.0.33-universal.jar) 
	GL info: ' Vendor: 'ATI Technologies Inc.' Version: '4.5.13399 Compatibility Profile Context 15.201.1151.1008' Renderer: 'AMD Radeon HD 6800 Series'
	Launched Version: MultiMC5
	LWJGL: 2.9.1
	OpenGL: AMD Radeon HD 6800 Series GL version 4.5.13399 Compatibility Profile Context 15.201.1151.1008, ATI Technologies Inc.
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)